const form = document.getElementById('form'),
  password = document.getElementById('password'),
  confirmPassword = document.getElementById('confirm-password'),
  email = document.getElementById('email'),
  confirmEmail = document.getElementById('confirm-email');
let isEmailValid = false,
  isPasswordValid = false;

function validateEmail() {
  if (email.value != confirmEmail.value) {
    confirmEmail.setCustomValidity('Konfirmasi email tidak sama!');
    isEmailValid = false;
  } else {
    confirmEmail.setCustomValidity('');
    isEmailValid = true;
  }
}

function validatePassword() {
  if (password.value != confirmPassword.value) {
    confirmPassword.setCustomValidity('Konfirmasi password tidak sama!');
    isPasswordValid = false;
  } else {
    confirmPassword.setCustomValidity('');
    isPasswordValid = true;
  }
}

email.onchange = validateEmail;
confirmEmail.onchange = validateEmail;
password.onchange = validatePassword;
confirmPassword.onkeyup = validatePassword;

validateEmail();
validatePassword();

form.addEventListener('submit', (e) => {
  e.preventDefault();
  if (isEmailValid && isPasswordValid) {
    return alert(true);
  }
  return alert(false);
});
