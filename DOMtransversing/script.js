const eldestParent = document.getElementById('eldest-parent');
eldestParent.firstElementChild.innerHTML = 'Diakses Melalui Eldest Parent';

const aChild = document.getElementById('a-child');
aChild.previousElementSibling.innerHTML = 'Diakses Melalui a Child';
aChild.nextElementSibling.innerHTML = 'Diakses Melalui a Child';

aChild.parentElement.parentElement.nextElementSibling.innerHTML = 'Diakses Melalui a Child';