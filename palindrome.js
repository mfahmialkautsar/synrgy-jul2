function palindrome(kata) {
  // you can only write your code here!
  function isPalindrome(s, i) {
    return (
      (i = i || 0) < 0 ||
      i >= s.length >> 1 ||
      (s[i] == s[s.length - 1 - i] && isPalindrome(s, ++i))
    );
  }
  return isPalindrome(kata.toLowerCase());
}

// TEST CASES
console.log(palindrome('katak')); // true
console.log(palindrome('blanket')); // false
console.log(palindrome('civic')); // true
console.log(palindrome('kasur rusak')); // true
console.log(palindrome('mister')); // false
